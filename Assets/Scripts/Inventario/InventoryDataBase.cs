﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Objetos/DataBase", order = 3)]
public class InventoryDataBase : ScriptableObject {

    [System.Serializable]
    public struct dataBaseObjects {
        public string nombre;
        public Sprite sprite;
        public Utilidad utilidad;
        public string descripcion;
        public string funcion;          //Acción que tendrá el objeto una vez se le haga clic, o se use.
        public string Hub;              //Nombre del 'Panel', Empty GameObjetc, donde se situará el objeto al equiparse.
        public string Slot;             //Nombre del Slot en el que se situará el objeto.
    }

    public enum Utilidad {
        acumulable,
        equipable,
    }

    public dataBaseObjects[] DBInventario;

}