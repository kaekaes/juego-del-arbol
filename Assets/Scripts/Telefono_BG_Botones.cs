﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Telefono_BG_Botones : MonoBehaviour, IPointerClickHandler {

    public Button BG_Button_ButtonCompontent;
    public GameObject BG_Object;
    public Image BG_Button_ImageCompontent;

    public void Start() {

        this.gameObject.GetComponent<Image>().sprite = BG_Button_ImageCompontent.sprite;

    }

    //al clicarse el sprite del boton será igual a BG_button (este viene de GUI_controller)
    public void OnPointerClick(PointerEventData eventData) {
        BG_Object.GetComponent<Image>().sprite = BG_Button_ImageCompontent.sprite;
    }
}
