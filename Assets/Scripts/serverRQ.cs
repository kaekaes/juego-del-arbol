﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


namespace serverRQ {

    #region CONSTANTS
    public static class CONSTANTS {
        private static string root = "https://cristiantechfolio.000webhostapp.com";
        public static string login = root + "/login.php";
        public static string banco = root + "/banco.php";
    }

    public enum accionBanco {
        sacar = 0,
        meter = 1,

    }
    #endregion

    #region login
    public class login {
        public bool running;
        public serverResponse respuesta;
        public login() {
            //respuesta = new serverResponse();
            respuesta.error = "-9999";
            respuesta.message = "Null socket.";
            running = false;
        }


        /*  
            Entrada: usuario y contrasena OPCIONAL bool, en caso de true muestra info en consola.
            Salida: ---
            Funcion: llamamos a login.php, comprobamos el usuario y la contrasena,
             y damos la info en la variable respuesta.
            Uso: nombreVariable.executeLogin("usuario","contrasena"); 
            es necesario using serverRQ;
        */

        // public IEnumerator View(string idPlayer, bool debug = false) {
        public IEnumerator executeLogin(string username, string password, bool debug = false) {
            WWWForm form = new WWWForm();
            form.AddField("passwordLogin", password);
            form.AddField("userLogin", username);

            WWW request = new WWW(CONSTANTS.login, form);
            yield return request;


            if (request.error == null) {
                respuesta = JsonUtility.FromJson<serverResponse>(request.text);
                yield return respuesta;
            } else
                Debug.Log(request.error);

            if (debug && request.error == null)
                Debug.Log(request.text);


        }
    }



    [System.Serializable]
    public struct serverResponse {

        public bool checkError() {
            if (this.error != "0") {
                return false;
            } else {
                return true;
            }
        }
        public string error;
        public string message;
        public inf info;
    }

    [System.Serializable]
    public struct inf {
        public string user;
        public string email;
        public int iduser;
    }
    #endregion


    #region items
    namespace items {


        [System.Serializable]
        public struct infItem {
            int id;
            int cantidad;
        }

        [System.Serializable]
        public struct inventario {
            public string error;
            public string message;

            public List<infItem> objetos;

        }

        public class inventarioServer {
            inventario Inventario;

            public bool checkError() {
                if (this.Inventario.error != "0") {
                    return false;
                } else {
                    return true;
                }
            }


            /*  
               Entrada: usuario y contrasena OPCIONAL bool, en caso de true muestra info en consola.
               Salida: ---
               Funcion: llamamos a login.php, comprobamos el usuario y la contrasena,
                y damos la info en la variable respuesta.
               Uso: StartCoroutine(nombreVariable.executeLogin("usuario","contrasena"));
               es necesario using serverRQ;
           */
            public IEnumerator getInventario(string idPlayer, bool debug = false) {
                WWWForm form = new WWWForm();
                form.AddField("idPlayerR", idPlayer);

                WWW request = new WWW("https://cristiantechfolio.000webhostapp.com/getItems.php", form);
                yield return request;

                if (request.error == null)
                    this.Inventario = JsonUtility.FromJson<inventario>(request.text);
                else
                    Debug.Log(request.error);

                if (debug && request.error == null)
                    Debug.Log(request.text);


            }


            public IEnumerator setInventario(string idPlayer, bool debug = false) {
                WWWForm form = new WWWForm();
                form.AddField("idPlayerR", idPlayer);

                WWW request = new WWW("https://cristiantechfolio.000webhostapp.com/getItems.php", form);
                yield return request;

                if (request.error == null)
                    this.Inventario = JsonUtility.FromJson<inventario>(request.text);
                else
                    Debug.Log(request.error);

                if (debug && request.error == null)
                    Debug.Log(request.text);


            }

        }



    }
    #endregion

    #region banco


    public class banco {
        public bool running;
        public bancoConection respuesta;
        public banco() {
            running = false;
            respuesta.error = "-9999";
            respuesta.message = "Null socket.";
        }

        /*  
           Entrada: idPlayer OPCIONAL bool, en caso de true muestra info en consola.
           Salida: ---
           Funcion: llamamos a banco.php, este devuelve el dinero de el player
           Uso: StartCoroutine(nombreVariable.View("idPlayer")); 
           es necesario using serverRQ;
       */
        public IEnumerator View(string idPlayer, bool debug = false) {
            running = true;
            WWWForm form = new WWWForm();
            form.AddField("unityPJ", idPlayer);
            form.AddField("ac", "view");

            WWW request = new WWW(CONSTANTS.banco, form);
            yield return request;

            if (request.error == null) {
                respuesta = JsonUtility.FromJson<bancoConection>(request.text);
                yield return respuesta;
                running = false;
            } else
                Debug.Log(request.error);

            if (debug && request.error == null)
                Debug.Log(request.text);

        }


        /*  
           Entrada: (string)idPlayer , (enum accionBanco)action, (float)cantidad OPCIONAL bool, en caso de true muestra info en consola.
           Salida: ---
           Funcion: llamamos a banco.php, en caso de accionBanco.sacar saca dinero y devuelve el dinero modificado,
           en caso de accionBanco.meter mete dinero al banco y devuelve el dinero modificado.
           Uso: StartCoroutine(nombreVariable.Add("idPlayer",accionBanco.sacar,1000)); 
           es necesario using serverRQ;
       */
        public IEnumerator Add(string idPlayer, accionBanco action, float cantidad, bool debug = false) {
            running = true;
            WWWForm form = new WWWForm();
            form.AddField("unityPJ", idPlayer);
            form.AddField("ac", (int)action == 0 ? "sacar" : "add");
            form.AddField("cantidad", cantidad.ToString());

            WWW request = new WWW(CONSTANTS.banco, form);
            yield return request;

            if (request.error == null) {
                respuesta = JsonUtility.FromJson<bancoConection>(request.text);
                yield return respuesta;
                running = false;
            } else
                Debug.Log(request.error);

            if (debug && request.error == null)
                Debug.Log(request.text);
        }
    }

    [System.Serializable]
    public struct bancoConection {
        public string error;
        public string message;
        public dineroInfo info;
    }
    [System.Serializable]
    public struct dineroInfo {
        public float dineroMano;
        public float dineroBanco;
    }
    #endregion
}
