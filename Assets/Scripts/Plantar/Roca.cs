﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roca : MonoBehaviour {

    public GameObject player, roca;

    //Vida de la roca
    public int Rock_HP;
    private int Rock_HP_default;

    public bool roto;

    //Tiempo para reaparecer
    public float Timer_Respawn;
    private float Timer_Respawn_default;

    //Tiempo de reaparicion de las roquitas
    public float Timer_Desaparicion;
    private float Timer_Desaparicion_default;

    //Arrays para las rocas
    public GameObject[] Piedra_GoArray;
    public Vector3[] Piedra_spawnarray_pos;
    public Quaternion[] Piedra_spawnarray_rot;
    public Rigidbody[] Piedra_RBArray;


    void Start() {
        roca = this.gameObject;
        Timer_Respawn_default = Timer_Respawn;
        Timer_Desaparicion_default = Timer_Desaparicion;

        Rock_HP_default = Rock_HP;
        Detectar_piedras();
    }

    void Update() {
        if (Rock_HP <= 0) {
            roca.GetComponent<MeshCollider>().enabled = false;
            roca.GetComponent<MeshRenderer>().enabled = false;
            Rotura();
        }

        if (Timer_Respawn <= 0) {
            roca.GetComponent<MeshCollider>().enabled = true;
            roca.GetComponent<MeshRenderer>().enabled = true;

            Rock_HP = Rock_HP_default;
            Timer_Desaparicion = Timer_Desaparicion_default;
            Timer_Respawn = Timer_Respawn_default;

            roto = false;
            for (int i = 0; i < transform.childCount; i++) {
                Piedra_RBArray[i].isKinematic = true;
                Piedra_GoArray[i].GetComponent<MeshCollider>().enabled = false;
                Piedra_GoArray[i].GetComponent<MeshRenderer>().enabled = false;
                Piedra_GoArray[i].transform.position = Piedra_spawnarray_pos[i];
                Piedra_GoArray[i].transform.rotation = Piedra_spawnarray_rot[i];
            }
        }
        if (Timer_Desaparicion <= 0) {
            for(int i = 0; i< transform.childCount; i++) {
                Piedra_RBArray[i].isKinematic = true;
                Piedra_GoArray[i].GetComponent<MeshCollider>().enabled = false;
                Piedra_GoArray[i].GetComponent<MeshRenderer>().enabled = false;
                Piedra_GoArray[i].transform.position = Piedra_spawnarray_pos[i];
                Piedra_GoArray[i].transform.rotation = Piedra_spawnarray_rot[i];
            }
        }
    }

    public void Rotura() {
        roto = true;
        for (int i = 0; i < transform.childCount; i++) {
            Piedra_RBArray[i].isKinematic = false;
            Piedra_GoArray[i].GetComponent<MeshCollider>().enabled = true;
            Piedra_GoArray[i].GetComponent<MeshRenderer>().enabled = true;
        }
        Timer_Respawn -= Time.deltaTime;
        Timer_Desaparicion -= Time.deltaTime;


    }
    public void Detectar_piedras() {

        Piedra_GoArray = new GameObject[transform.childCount];
        Piedra_spawnarray_pos = new Vector3[transform.childCount];
        Piedra_spawnarray_rot = new Quaternion[transform.childCount];
        Piedra_RBArray = new Rigidbody[transform.childCount];
        
        for(int i = 0; i < transform.childCount; i++) {
            Piedra_GoArray[i] = transform.GetChild(i).gameObject;
            Piedra_spawnarray_pos[i] = transform.GetChild(i).transform.position;
            Piedra_spawnarray_rot[i] = transform.GetChild(i).transform.rotation;
            Piedra_RBArray[i] = transform.GetChild(i).GetComponent<Rigidbody>();
        }

    }
}
