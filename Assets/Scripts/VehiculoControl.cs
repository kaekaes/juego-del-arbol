﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehiculoControl : MonoBehaviour {

    public WheelCollider DelanteraDerecha, DelanteraIzquierda, TraseraDerecha, TraseraIzquierda;
    public float VelMotor;
    public float AnguloRueda;
    public float FuerzaFreno;
    float AntiRoll = 5000f;
    public Rigidbody Rg;
    public bool Encendido;

    void FixedUpdate()
    {
        if (Encendido == true) {
        ControlDelantera();
        Estabilizador();
        Freno();
        }
    }

    void ControlDelantera()
    {
        float Vel = VelMotor * Input.GetAxis("Vertical");
        float Angulo = AnguloRueda * Input.GetAxis("Horizontal");
        DelanteraDerecha.motorTorque = Vel;
        DelanteraIzquierda.motorTorque = Vel;
        DelanteraDerecha.steerAngle = Angulo;
        DelanteraIzquierda.steerAngle = Angulo;
    }

    void Freno()
    {
        float Freno = FuerzaFreno * Input.GetAxisRaw("Jump");
        DelanteraDerecha.brakeTorque = Freno;
        DelanteraIzquierda.brakeTorque = Freno;
        DelanteraDerecha.brakeTorque = Freno;
        DelanteraIzquierda.brakeTorque = Freno;
    }
    void Estabilizador()
    {
        WheelHit hit;
        float travelL = 1.0f;
        float travelR = 1.0f;

        bool groundedL = DelanteraIzquierda.GetGroundHit(out hit);
        if (groundedL)
            travelL = (-DelanteraIzquierda.transform.InverseTransformPoint(hit.point).y - DelanteraIzquierda.radius) / DelanteraIzquierda.suspensionDistance;

        bool groundedR = DelanteraDerecha.GetGroundHit(out hit);
        if (groundedR)
            travelR = (-DelanteraDerecha.transform.InverseTransformPoint(hit.point).y - DelanteraDerecha.radius) / DelanteraDerecha.suspensionDistance;

        float antiRollForce = (travelL - travelR) * AntiRoll;
        if (groundedL)
            Rg.AddForceAtPosition(DelanteraIzquierda.transform.up * -antiRollForce, DelanteraIzquierda.transform.position);
        if (groundedR)
            Rg.AddForceAtPosition(DelanteraDerecha.transform.up * antiRollForce, DelanteraDerecha.transform.position);
    }
}
