﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaHambre : MonoBehaviour
{

    public int Hambre = 100, Sed = 50;
    public float TiempoHambre = 30, TiempoSed = 30;
    int VidaActual = 100;
    private int maxhealth = 100;
    public Slider VidaSlider;
    public Slider HambreSlider;
    public Slider SedSlider;
    public int VidaInicial;
    public GameObject Hud;
    public Text UIText;
    public float time = 300;
    public GameObject Muerto;
    float UltimoTiempoHambre;
    float UltimoTiempoSed;
    private Animator myAnimator;

    public void Start()
    {
        Hud.SetActive(true);
        Muerto.SetActive(false);
        VidaSlider.value = VidaActual;
        VidaActual = VidaInicial;
        HambreSlider.value = Hambre;
        SedSlider.value = Sed;
        myAnimator = GetComponent<Animator>();
    }
    private void Update()
    {
        Sed = HambreSed(Sed, TiempoSed, ref UltimoTiempoSed);
        Hambre = HambreSed(Hambre, TiempoHambre, ref UltimoTiempoHambre);
        VidaSlider.value = VidaActual;
        HambreSlider.value = Hambre;
        SedSlider.value = Sed;
        UIText.text = "Reapareces en:" + " " + time.ToString("f0");
        if (VidaActual > maxhealth)
        {
            VidaActual = 100;
        }
        if (VidaActual <= 0)
        {
            time -= Time.deltaTime;
            Muerto.SetActive(true);
            myAnimator.SetLayerWeight(1, 1f);
            myAnimator.SetInteger("CurrentAction", 6);
        }
    }

    int HambreSed(int Perder, float Tiempo, ref float UltimoTiempo)
    {
        if (UltimoTiempo + Tiempo < Time.time)
        {
            UltimoTiempo = Time.time;
            if (Perder > 0) Perder--;
            else VidaActual--;
        }
        return Perder;
    }
}
