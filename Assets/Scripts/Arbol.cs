﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arbol : MonoBehaviour {

    //Tiempo de caida
    public float Timer_Respawn = 300;
    private float Timer_Respawn_default;

    //Tiempo de desaparición
    public float Timer_caida = 7;
    private float Timer_caida_default;

    //Tree_HP necesarios para tirar el arbol
    public int Tree_HP = 10;
    private int Tree_HP_default;

    //este mismo objeto de arbol y el de player que le clica
    //(aunque arbol se pone el Void Start, player se pone desde el script de player)
    public GameObject arbol;
    public GameObject player;
    public Rigidbody rb;

    //estado de caido y preparado para respawn
    public bool Caido;
    public bool respawneable;

    //posicion y rotacion del arbol al inicio y la de cuando golpea el arbol
    public Vector3 pos_I, pos_A;
    public Quaternion rot_I, rot_A;

    //Textura/modelo del arbol
    public MeshRenderer tree_mesh;
    public CapsuleCollider tree_collider;

    //objeto para instantiate el tronco
    public GameObject Madera_Go;

    //GameObject de la madera y las posiciones de los spawns
    public GameObject[] Madera_GoArray;
    public Vector3[] Madera_spawnarray_pos;
    public Quaternion[] Madera_spawnarray_rot;
    public bool Madera_Dropeable = true;



    void Start() {
        //Pongo la posicion y la rotacion como está de inicio, y hago que no se pueda mover junto con los diferentes
        //Getcomponents necesarios
        pos_I = transform.position;
        rot_I = transform.rotation;
        arbol = this.gameObject;
        rb = this.gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        tree_mesh = GetComponent<MeshRenderer>();
        tree_collider = GetComponent<CapsuleCollider>();

        //Asginar unos default para cada vez que respawnea
        Timer_Respawn_default = Timer_Respawn;
        Timer_caida_default = Timer_caida;
        Tree_HP_default = Tree_HP;

        //este void sirve para ver y meter las maderas que estén como child dentro de un array
        Reconocer_maderas();
    }

    void Update() {
        //Si ya ha recibido los Tree_HP necesarios, que caiga un un empujon y vayamos a void Arbol_Caido()
        if (Tree_HP <= 0) {
            rb.isKinematic = false;
            rb.AddForce(player.transform.forward * 8);
            pos_A = player.transform.position;
            rot_A = player.transform.rotation;
            Arbol_Caido();
        }
        //Si está caido, que empiece el timer que lo destruye y crea la madera que suelta.
        if (Caido) {
            Timer_caida -= Time.deltaTime;
        }
        //Si ya ha caido empieza el timer y si termina lo resetéa tal y como estaba antes de caer.
        if (respawneable) {
            Timer_Respawn -= Time.deltaTime;
            Timer_caida = Timer_caida_default;
            Caido = false;
            rb.isKinematic = true;
        }
        if (Timer_Respawn <= 0) {
            respawneable = false;
            Tree_HP = Tree_HP_default;
            tree_mesh.enabled = true;
            tree_collider.enabled = true;
            transform.position = pos_I;
            transform.rotation = rot_I;
            Timer_Respawn = Timer_Respawn_default;
            Madera_Dropeable = true;

            //crear los troncos cuando reaparece
            GameObject madera_1 = Instantiate(Madera_Go, Madera_spawnarray_pos[0], Madera_spawnarray_rot[0]);
            GameObject madera_2 = Instantiate(Madera_Go, Madera_spawnarray_pos[1], Madera_spawnarray_rot[1]);
            GameObject madera_3 = Instantiate(Madera_Go, Madera_spawnarray_pos[2], Madera_spawnarray_rot[2]);
            //hacerlos child del arbol que es
            madera_1.transform.parent = transform;
            madera_2.transform.parent = transform;
            madera_3.transform.parent = transform;
            //hacer que no se muevan
            madera_1.GetComponent<Rigidbody>().isKinematic = true;
            madera_2.GetComponent<Rigidbody>().isKinematic = true;
            madera_3.GetComponent<Rigidbody>().isKinematic = true;

            madera_1.GetComponent<MeshRenderer>().enabled = false;
            madera_2.GetComponent<MeshRenderer>().enabled = false;
            madera_3.GetComponent<MeshRenderer>().enabled = false;
            madera_1.GetComponent<MeshCollider>().enabled = false;
            madera_2.GetComponent<MeshCollider>().enabled = false;
            madera_3.GetComponent<MeshCollider>().enabled = false;

            Reconocer_maderas();
        }
    }

    public void Arbol_Caido() {
        //si ha caido, que pase a estado caido y cree y desactive su modelo a la vez que lo pone bien.
        Caido = true;
        if (Timer_caida <= 0) {
            tree_mesh.enabled = false;
            tree_collider.enabled = false;
            Madera();
            respawneable = true;
            rb.isKinematic = true;
        }
    }

    //dropea la madera
    public void Madera() {
        if (Madera_Dropeable) {
            //activar el modelo y la colision
            Madera_GoArray[0].GetComponent<MeshRenderer>().enabled = true;
            Madera_GoArray[1].GetComponent<MeshRenderer>().enabled = true;
            Madera_GoArray[2].GetComponent<MeshRenderer>().enabled = true;

            Madera_GoArray[0].GetComponent<MeshCollider>().enabled = true;
            Madera_GoArray[1].GetComponent<MeshCollider>().enabled = true;
            Madera_GoArray[2].GetComponent<MeshCollider>().enabled = true;

            Madera_GoArray[0].GetComponent<Rigidbody>().isKinematic = false;
            Madera_GoArray[1].GetComponent<Rigidbody>().isKinematic = false;
            Madera_GoArray[2].GetComponent<Rigidbody>().isKinematic = false;

            //sacarlo del parent
            Madera_GoArray[0].transform.parent = GameObject.Find("Arboles").transform;
            Madera_GoArray[1].transform.parent = GameObject.Find("Arboles").transform;
            Madera_GoArray[2].transform.parent = GameObject.Find("Arboles").transform;
            
            Madera_Dropeable = false;
        }
    }
    public void Reconocer_maderas() {
        //hacer que los child entren en el array, y guardar sus posiciones
        Madera_GoArray = new GameObject[transform.childCount];
        Madera_spawnarray_pos = new Vector3[transform.childCount];
        Madera_spawnarray_rot = new Quaternion[transform.childCount];
        for (int i = 0; i < transform.childCount; i++) {
            Madera_GoArray[i] = transform.GetChild(i).gameObject;
            Madera_spawnarray_pos[i] = Madera_GoArray[i].transform.position;
            Madera_spawnarray_rot[i] = Madera_GoArray[i].transform.rotation;
        }
    }
}